Servlets/ Filter/Session

Description:
An application has a registration page. The correct logins are "user" and "admin". The field "password" is not important.
You need to create handlers for them. For that create 2 servlets, listener and filter 

+ AuthFilter
+ ContextListener
+ LoginServlet
+ LogoutServlet


Details</br>
AuthFilter should:

+ placed in com.mjc.task.filter package
+ check the session attribute "user" for paths "/user*". If there isn't redirect the request to the "/login.jsp" page

ContextListener should:

+ placed in com.mjc.task.filter package
+ set "servletTimeInit"  attribute in the context, "servletTimeInit" is a date & time when context initialized  

LoginServlet should:
+ placed in com.mjc.task.filter package
+ have url "/login"
+ check the session attribute "user"  
+ for get request if the session attribute "user" exists, redirect to the "/login.jsp" page, else redirect to the  
+ "/user/hello.jsp"
+ for post request check the request attribute "login" in users (Users.class) and the request attribute "password" on null. If attributes 
are correct set session attribute "user" and forward  "/user/hello.jsp" else forward the request to the "/login.jsp". </br>

LogoutServlet should:
+ placed in com.mjc.task.filter package
+ have url "/logout
+ delete session attribute "user"
+ invalidate session
+ redirect "/login.jsp"