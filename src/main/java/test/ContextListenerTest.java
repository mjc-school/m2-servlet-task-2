package test;

import mjc.servlets.task.filter.ContextListener;
import org.junit.jupiter.api.Test;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

class ContextListenerTest {
    @Test
    public void testListenerHasAnnotation() {
        Annotation actual = ContextListener.class.getAnnotation(WebListener.class);
        assertNotEquals(null, actual, "Listeners should have WebListener annotation");
    }

    @Test
    public void testListenerImplementInterface() {
        boolean actual = ServletContextListener.class.isAssignableFrom(ContextListener.class);
        assertTrue(actual, "ContextListener.class should implement  ServletContextListener interface");
    }

    private Method getContextInitializedMethod() {
        Method actual = null;
        try {
            actual =
                    ContextListener.class.getDeclaredMethod(
                            "contextInitialized", ServletContextEvent.class);
        } catch (NoSuchMethodException e) {
            assertTrue(false, "ContextListener should have contextInitialized method ");
        }
        return actual;
    }

    @Test
    public void testContextInitialized() throws Exception {
        ServletContextEvent contextEvent = mock(ServletContextEvent.class);
        ServletContext context = mock(ServletContext.class);
        ContextListener contextListener = new ContextListener();
        when(contextEvent.getServletContext()).thenReturn(context);
        Method contextInitialized = getContextInitializedMethod();
        contextInitialized.invoke(contextListener, contextEvent);

        verify(context).setAttribute(anyString(), anyString());
    }
}