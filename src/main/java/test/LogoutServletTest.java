package test;

import mjc.servlets.task.filter.LogoutServlet;
import org.junit.jupiter.api.Test;

import javax.servlet.RequestDispatcher;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class LogoutServletTest {

    @Test
    public void testServletHasAnnotation() {
        Annotation actual = LogoutServlet.class.getAnnotation(WebServlet.class);
        assertNotEquals(null, actual, "Servlet should have WebServlet annotation");
    }

    @Test
    public void testServletInheritedFromHttpServlet() {
        Class actual = LogoutServlet.class.getSuperclass();
        assertEquals(HttpServlet.class, actual, "Servlet should be inherited from HttpServlet");
    }

    private Method getDoGetMethod() {
        Method actual = null;
        try {
            actual =
                    LogoutServlet.class.getDeclaredMethod(
                            "doGet", HttpServletRequest.class, HttpServletResponse.class);
            actual.setAccessible(true);
        } catch (NoSuchMethodException e) {
            assertTrue(false, "Servlet should have doGet method implemented for logout users by link");
        }
        return actual;
    }

    @Test
    public void testLogout() throws Exception {
        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletResponse response = mock(HttpServletResponse.class);
        RequestDispatcher dispatcher = mock(RequestDispatcher.class);
        HttpSession session = mock(HttpSession.class);
        LogoutServlet servlet = new LogoutServlet();

        when(request.getRequestDispatcher(anyString())).thenReturn(dispatcher);
        when(request.getSession()).thenReturn(session);
        Method doGet = getDoGetMethod();
        doGet.invoke(servlet, request, response);

        verify(session).removeAttribute("user");
        verify(session).invalidate();
        verify(response).sendRedirect(anyString());
    }
}