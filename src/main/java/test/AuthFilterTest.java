package test;

import mjc.servlets.task.filter.AuthFilter;
import org.junit.jupiter.api.Test;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

class AuthFilterTest {
    @Test
    public void testFilterHasAnnotation() {
        Annotation actual = AuthFilter.class.getAnnotation(WebFilter.class);
        assertNotEquals(null, actual, "Filter should have WebFilter annotation");
    }

    @Test
    public void testFilterImplementInterface() {
        boolean actual = Filter.class.isAssignableFrom(AuthFilter.class);
        assertTrue(actual, "AuthFilter should implement  Filter interface");
    }

    private Method getDoFilterMethod() {
        Method actual = null;
        try {
            actual =
                    AuthFilter.class.getDeclaredMethod(
                            "doFilter", ServletRequest.class, ServletResponse.class, FilterChain.class);
        } catch (NoSuchMethodException e) {
            assertTrue(false, "Filter should have doFilter method implemented for displaying users");
        }
        return actual;
    }

    @Test
    public void testDoFilter() throws Exception {
        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletResponse response = mock(HttpServletResponse.class);
        FilterChain filterChain = mock(FilterChain.class);
        HttpSession session = mock(HttpSession.class);
        AuthFilter authFilter = new AuthFilter();

        when(request.getSession()).thenReturn(session);
        when(session.getAttribute("user")).thenReturn(null);
        Method doFilter = getDoFilterMethod();
        doFilter.invoke(authFilter, request, response, filterChain);

        verify(response).sendRedirect(request.getContextPath()+"/login.jsp");
    }
}