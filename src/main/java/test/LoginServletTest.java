package test;

import mjc.servlets.task.filter.LoginServlet;
import org.junit.jupiter.api.Test;

import javax.servlet.RequestDispatcher;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

class LoginServletTest {
    @Test
    public void testServletHasAnnotation() {
        Annotation actual = LoginServlet.class.getAnnotation(WebServlet.class);
        assertNotEquals(null, actual, "Servlet should have WebServlet annotation");
    }

    @Test
    public void testServletInheritedFromHttpServlet() {
        Class actual = LoginServlet.class.getSuperclass();
        assertEquals(HttpServlet.class, actual, "Servlet should be inherited from HttpServlet");
    }

    private Method getMethod(String method) {
        String reason = "";
        if (method.equals("doGet")) {
            reason = "for navigating to /add page";
        } else if (method.equals("doPost")) {
            reason = "for adding a new user";
        }
        Method actual = null;
        try {
            actual =
                    LoginServlet.class.getDeclaredMethod(
                            method, HttpServletRequest.class, HttpServletResponse.class);
            actual.setAccessible(true);
        } catch (NoSuchMethodException e) {
            assertTrue(false, "Servlet should have " + method + " method implemented " + reason);
        }
        return actual;
    }


    @Test
    public void testGetLoginWithAuthUser() throws Exception {
        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletResponse response = mock(HttpServletResponse.class);
        HttpSession session = mock(HttpSession.class);
        LoginServlet servlet = new LoginServlet();
        String user= "user";

        when(request.getSession()).thenReturn(session);
        when(session.getAttribute("user")).thenReturn(user);

        Method doGet = getMethod("doGet");
        doGet.invoke(servlet, request, response);

        verify(response).sendRedirect(request.getContextPath()+"/user/hello.jsp");
    }

    @Test
    public void testGetLoginWithoutAuthUser() throws Exception {
        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletResponse response = mock(HttpServletResponse.class);
        HttpSession session = mock(HttpSession.class);
        LoginServlet servlet = new LoginServlet();

        when(request.getSession()).thenReturn(session);
        when(session.getAttribute("user")).thenReturn(null);
        Method doGet = getMethod("doGet");
        doGet.invoke(servlet, request, response);

        verify(response).sendRedirect(request.getContextPath()+"/login.jsp");
    }
    @Test
    public void testPostLoginWithCorrectPassword() throws Exception {
        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletResponse response = mock(HttpServletResponse.class);
        RequestDispatcher dispatcher = mock(RequestDispatcher.class);
        HttpSession session = mock(HttpSession.class);
        LoginServlet servlet = new LoginServlet();
        String user = "user";

        when(request.getParameter("login")).thenReturn(user);
        when(request.getParameter("password")).thenReturn("pass");
        when(request.getRequestDispatcher("/user/hello.jsp")).thenReturn(dispatcher);
        when(request.getSession()).thenReturn(session);
        Method doPost = getMethod("doPost");
        doPost.invoke(servlet, request, response);

        verify(session).setAttribute("user", user);
        verify(dispatcher).forward(request, response);
    }

    @Test
    public void testPostLoginWithBlankPasswor() throws Exception {
        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletResponse response = mock(HttpServletResponse.class);
        RequestDispatcher dispatcher = mock(RequestDispatcher.class);
        LoginServlet servlet = new LoginServlet();
        String user = "user";

        when(request.getParameter("login")).thenReturn(user);
        when(request.getParameter("password")).thenReturn("");
        when(request.getRequestDispatcher("/login.jsp")).thenReturn(dispatcher);
        Method doPost = getMethod("doPost");
        doPost.invoke(servlet, request, response);

        verify(dispatcher).forward(request, response);
    }
}