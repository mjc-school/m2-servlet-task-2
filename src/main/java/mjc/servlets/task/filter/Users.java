package mjc.servlets.task.filter;

import java.util.ArrayList;
import java.util.Collections;;
import java.util.List;

public enum Users {
    INSTANCE;
    private List<String> users;

    Users() {
        users = new ArrayList<>();
        users.add("admin");
        users.add("user");
    }

    public List<String> getUsers() {
        return Collections.unmodifiableList(users);
    }
}
